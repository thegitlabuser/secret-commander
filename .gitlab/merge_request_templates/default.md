## Merge request checklist

This MR fulfills the following requirements:

- Tests for the changes have been added (for bug fixes / features)
- All automated checks have passed
- Docs have been reviewed and added / updated

## Merge request type

Type of change this MR introduces:

- [ ] Bugfix
- [ ] Feature
- [ ] Code style update (formatting, renaming)
- [ ] Refactoring (no functional changes, no api changes)
- [ ] Build related changes
- [ ] Documentation content changes

## Does this introduce a breaking change?

- Yes
- No

## Description

A clear and concise description.

## Related PRs

Link to related PR/PRs

## Related User Stories

Link to related US

## To Reproduce

Steps to reproduce the behavior:
(eg: Go to '...'; Click on '....'; See error; )

### Expected behavior

A clear and concise description of what you expected to happen.

### Screenshots

If applicable, add screenshots to help explain your problem.
⬅️  _before_
➡️  _after_

## Checklist

- [ ] I have checked that the code fulfills the coding guidelines
- [ ] I have performed a self-review of my own code
- [ ] I have commented my code, particularly in hard-to-understand areas
- [ ] I have made corresponding changes to the documentation
- [ ] I have added tests that prove my fix is effective or that my feature works
- [ ] I have updated the CHANGELOG.md with details about my changes
