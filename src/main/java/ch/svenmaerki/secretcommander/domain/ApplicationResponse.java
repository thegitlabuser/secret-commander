package ch.svenmaerki.secretcommander.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

import java.time.Instant;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.ALWAYS;

@Data
@SuperBuilder
@JsonInclude(ALWAYS)
public class ApplicationResponse {

    @Builder.Default
    protected final String timeStamp = Instant.now().toString();

    protected final int statusCode;

    protected final HttpStatus status;

    protected final String message;

    protected final Object data;

}