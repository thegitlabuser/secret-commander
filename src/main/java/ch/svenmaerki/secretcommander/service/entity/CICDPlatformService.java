package ch.svenmaerki.secretcommander.service.entity;

import ch.svenmaerki.secretcommander.domain.entity.CICDPlatform;
import ch.svenmaerki.secretcommander.repository.entity.CICDPlatformRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CICDPlatformService {

    private final CICDPlatformRepository cicdPlatformRepository;

    /**
     * Returns all CI CD platforms
     *
     * @return The CI CD platforms
     */
    public List<CICDPlatform> findAll() {
        return cicdPlatformRepository.findAll();
    }

}
