package ch.svenmaerki.secretcommander.service.entity;

import ch.svenmaerki.secretcommander.domain.entity.AccessToken;
import ch.svenmaerki.secretcommander.repository.entity.AccessTokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccessTokenService {

    private final AccessTokenRepository accessTokenRepository;

    /**
     * Returns all access tokens
     *
     * @return The access tokens
     */
    public List<AccessToken> findAll() {
        return accessTokenRepository.findAll();
    }

}
