package ch.svenmaerki.secretcommander.controller.authentication;

import ch.svenmaerki.secretcommander.domain.ApplicationResponse;
import ch.svenmaerki.secretcommander.domain.authentication.AuthenticationRequest;
import ch.svenmaerki.secretcommander.domain.authentication.RegisterRequest;
import ch.svenmaerki.secretcommander.service.authentication.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthenticationController {

    private final AuthenticationService service;

    @PostMapping("/register")
    public ResponseEntity<ApplicationResponse> register(
            @RequestBody RegisterRequest request
    ) {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("Registered successfully.")
                .data(service.register(request))
                .build();

        return ResponseEntity.ok(response);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<ApplicationResponse> authenticate(
            @RequestBody AuthenticationRequest request
    ) {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("Registered successfully.")
                .data(service.authenticate(request))
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/refresh")
    public ResponseEntity<ApplicationResponse> refresh(
            @RequestHeader("Authorization") String token
    ) {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("Registered successfully.")
                .data(service.refresh(token))
                .build();

        return ResponseEntity.ok(response);
    }

}
