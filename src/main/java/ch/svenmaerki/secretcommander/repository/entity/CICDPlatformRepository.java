package ch.svenmaerki.secretcommander.repository.entity;

import ch.svenmaerki.secretcommander.domain.entity.CICDPlatform;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CICDPlatformRepository extends JpaRepository<CICDPlatform, Long> {

    List<CICDPlatform> findAll();

}
