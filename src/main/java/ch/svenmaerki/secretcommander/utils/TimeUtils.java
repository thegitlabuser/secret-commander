package ch.svenmaerki.secretcommander.utils;

import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class TimeUtils {

    /**
     * Formats a duration in a human readable format.
     * Example output: 1 day, 2 hours, 3 minutes, 4 seconds
     *
     * @param duration The duration to format.
     * @return The formatted duration.
     */
    public static String formatDurationWords(Duration duration) {
        long days = duration.toDays();
        long hours = duration.toHours() % 24;
        long minutes = duration.toMinutes() % 60;
        long seconds = duration.getSeconds() % 60;

        StringBuilder sb = new StringBuilder();
        if (days > 0) {
            sb.append(days).append(" day");
            if (days > 1) {
                sb.append("s");
            }
            sb.append(", ");
        }
        if (hours > 0) {
            sb.append(hours).append(" hour");
            if (hours > 1) {
                sb.append("s");
            }
            sb.append(", ");
        }
        if (minutes > 0) {
            sb.append(minutes).append(" minute");
            if (minutes > 1) {
                sb.append("s");
            }
            sb.append(", ");
        }
        sb.append(seconds).append(" second");
        if (seconds != 1) {
            sb.append("s");
        }
        return sb.toString();
    }

}
