stages:
  - compile
  - docker
  - quality
  - visualize
  - release

default:
  interruptible: true

.docker-build-and-push-template: &docker-build-and-push-template
  image: docker:latest
  needs: [ "maven" ]
  dependencies: [ "maven" ]
  rules:
    - if: $CI_COMMIT_TAG
  services:
    - docker:dind
  script:
    - docker build -t $DOCKER_IMAGE_TAG .
    - echo $DOCKER_PASSWORD | docker login -u $DOCKER_USER --password-stdin $DOCKER_REGISTRY
    - docker push $DOCKER_IMAGE_TAG
    - docker logout

variables:
  MAVEN_CLI_OPTS: "-Dmaven.repo.local=.m2/repository --batch-mode --errors"
  DOCKERHUB_IMAGE: "index.docker.io/svenmaerki/secret-commander"

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - when: always

include:
  - template: Code-Quality.gitlab-ci.yml

cache:
  key: mvn
  paths:
    - .m2/repository

######### Compile #########

maven:
  image: maven:3.8.7-openjdk-18-slim
  stage: compile
  cache:
    key: mvn
    paths:
      - .m2/repository
  script:
    - mvn dependency:go-offline $MAVEN_CLI_OPTS
    - mvn package $MAVEN_CLI_OPTS

######### Docker #########

docker-build-and-push-registry:
  <<: *docker-build-and-push-template
  stage: docker
  variables:
    DOCKER_REGISTRY: $CI_REGISTRY
    DOCKER_USER: $CI_REGISTRY_USER
    DOCKER_PASSWORD: $CI_REGISTRY_PASSWORD
    DOCKER_IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

docker-build-and-push-dockerhub:
  <<: *docker-build-and-push-template
  stage: docker
  variables:
    DOCKER_REGISTRY: $DOCKERHUB_REGISTRY
    DOCKER_USER: $DOCKERHUB_USER
    DOCKER_PASSWORD: $DOCKERHUB_PASSWORD
    DOCKER_IMAGE_TAG: $DOCKERHUB_IMAGE:$CI_COMMIT_TAG

######### Quality #########

container-scanning-trify:
  image: docker:stable
  services:
    - name: docker:dind
      entrypoint: [ "env", "-u", "DOCKER_HOST" ]
      command: [ "dockerd-entrypoint.sh" ]
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    IMAGE: trivy-ci-test:$CI_COMMIT_SHA
    TRIVY_NO_PROGRESS: "true"
    TRIVY_CACHE_DIR: ".trivycache/"
  stage: quality
  cache:
    paths:
      - .trivycache/
  before_script:
    - export TRIVY_VERSION=$(wget -qO - "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
    - echo $TRIVY_VERSION
    - wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -O - | tar -zxvf -
  allow_failure:
    exit_codes: 10
  script:
    # Build image
    - docker build -t $IMAGE .
    # update vulnerabilities db
    - ./trivy image --download-db-only --cache-dir .trivycache/
    # Build report
    - ./trivy image --exit-code 0 --format template --template "@contrib/gitlab.tpl" -o gl-container-scanning-report.json $IMAGE --cache-dir .trivycache/
    # Print report
    - ./trivy image --exit-code 0 --severity HIGH $IMAGE --cache-dir .trivycache/
    # Fail on severe vulnerabilities
    - ./trivy image --exit-code 10 --severity CRITICAL $IMAGE --cache-dir .trivycache/
  artifacts:
    expire_in: 30 days
    reports:
      container_scanning: gl-container-scanning-report.json

code_quality:
  stage: quality
  artifacts:
    expire_in: 30 days
    paths: [ gl-code-quality-report.json ]
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never

code-coverage:
  stage: quality
  image: maven:3.8.7-openjdk-18-slim
  script:
    - mvn $MAVEN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report
  artifacts:
    expire_in: 30 days
    paths:
      - target/site/jacoco/jacoco.xml
  coverage: /Total.*?([0-9]{1,3})%/

visualize:
  stage: visualize
  image: registry.gitlab.com/haynes/jacoco2cobertura:1.0.7
  script:
    # convert report from jacoco to cobertura, using relative project path
    - python /opt/cover2cover.py target/site/jacoco/jacoco.xml $CI_PROJECT_DIR/src/main/java/ > target/site/cobertura.xml
  needs: [ "code-coverage" ]
  artifacts:
    expire_in: 30 days
    reports:
      coverage_report:
        coverage_format: cobertura
        path: target/site/cobertura.xml

######### Release #########

release:
  image: maven:3.8.7-openjdk-18-slim
  stage: release
  when: manual
  cache:
    key: mvn
    paths:
      - .m2/repository
  interruptible: false
  before_script:
    # Installing git
    - apt-get update && apt-get install -y git

    # Configuring git user
    - git config user.name $CI_PROJECT_NAME
    - git config user.email $GITLAB_USER_EMAIL
    - git config pull.rebase true

    # Configuring git remote
    - CI_PUSH_REPO=`git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g"`
    - git remote set-url --push origin "https://$CI_PROJECT_NAME:$GIT_TOKEN@$CI_PUSH_REPO"
    - git fetch
    - git checkout $CI_COMMIT_REF_NAME
    - git pull origin $CI_COMMIT_REF_NAME
  script:
    - mvn $MAVEN_BUILD_OPTS -B -DpushChanges=false -DautoVersionSubmodules=true -DskipTests=true -Dmaven.test.skip=true -Darguments=-DskipTests release:prepare
    - git push
    - git push --tags
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
