package ch.svenmaerki.secretcommander.controller.entity;

import ch.svenmaerki.secretcommander.domain.ApplicationResponse;
import ch.svenmaerki.secretcommander.service.entity.RepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/repository")
public class RepositoryController {

    private final RepositoryService repositoryService;

    @GetMapping
    public ResponseEntity<ApplicationResponse> getRepositories() {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("All repositories fetched.")
                .data(repositoryService.findAll())
                .build();

        return ResponseEntity.ok(response);
    }

}
