# Pipeline Secrets

Pipeline Secrets is a Java Spring Boot application that lets users manage secrets in their CI/CD pipeline from a single application. With
Pipeline Secrets, users can view, edit, and delete secrets for all their pipeline stages in one convenient interface, making it easier to manage and
secure their pipeline.

## Getting Started

To get started with Pipeline Secrets, follow these steps:

1. Clone the repository: `git clone https://gitlab.com/svenmaerki/secret-commander.git`
2. Install the necessary dependencies: `mvn install`
3. Start the application: `mvn spring-boot:run`
4. Once the application is running, you can access it by navigating to `http://localhost:8080` in your web browser.

## Usage

To use Pipeline Secrets, users must first authenticate with the application using their CI/CD platform credentials. Once authenticated, they can view
a
list of all their pipeline stages and the secrets associated with each stage. They can then edit or delete individual secrets as needed.

Pipeline Secrets also provides an API for programmatic access to secrets. Users can use the API to retrieve secrets for use in their pipeline scripts,
or to create new secrets programmatically.

## Authentication

Pipeline Secrets uses OAuth 2.0 for authentication. Currently, the following CI/CD platforms are supported:

- [GitLab](https://gitlab.com)
- [GitHub](https://github.com)
- [Bitbucket](https://bitbucket.org)
- [Azure DevOps](https://azure.microsoft.com/en-us/services/devops/)
- [Jenkins](https://jenkins.io)
- [Bamboo](https://www.atlassian.com/software/bamboo)
- [ArgoCD](https://argoproj.github.io/argo-cd/)

To authenticate with Pipeline Secrets, users must first create an OAuth application on their CI/CD platform. Once the application is created, users
can use the application's client ID and client secret to authenticate with Pipeline Secrets.

## Configuration

Pipeline Secrets can be configured using environment variables or a application.yml file. The following configuration options are available:

- `server.port`: The port on which the application will run (default: `8080`).
- `spring.datasource.url`: The URL for the database connection.
- `spring.datasource.username`: The username for the database connection.
- `spring.datasource.password`: The password for the database connection.
- `spring.jpa.hibernate.ddl-auto`: The Hibernate DDL mode (default: update).
- `spring.jpa.show-sql`: Whether to show SQL statements in the logs (default: false).
- `spring.jpa.properties.hibernate.dialect`: The Hibernate dialect to use.
- `spring.jpa.properties.hibernate.format_sql`: Whether to format SQL statements in the logs (default: false).

## API

Pipeline Secrets provides a REST API for programmatic access to secrets. The API is documented using Open AI Specification, and can be accessed by
navigating to
`http://localhost:8080/swagger-ui.html` in your web browser.

## Testing

Pipeline Secrets uses JUnit 5 for testing. To run the test suite, run the following command:

`mvn test`

## Contributing

If you'd like to contribute to Pipeline Secrets, feel free to fork the repository and submit a pull request. Before submitting a pull request, please
make sure that your changes are fully tested and that they conform to the existing code style and standards.
