package ch.svenmaerki.secretcommander.domain.authentication;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthenticationResponse {

    private final String token;

}