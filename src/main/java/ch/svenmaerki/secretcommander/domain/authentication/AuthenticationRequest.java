package ch.svenmaerki.secretcommander.domain.authentication;

import lombok.Data;

@Data
public class AuthenticationRequest {

    private final String username;

    private final String password;

}
