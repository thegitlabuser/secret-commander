FROM maven:3.8.7-openjdk-18-slim AS build

LABEL version="1.0.0"
LABEL maintainer="Sven Märki sven.maerki@gmx.ch"
LABEL description="Secret Commander Backend Docker Image"

ENV APP_HOME=/usr/app
ENV MAVEN_OPTS="-Dmaven.repo.local=/tmp/m2repo"
ENV MAVEN_CLI_OPTS="--batch-mode --errors --fail-at-end --show-version"

WORKDIR /build

COPY pom.xml .

RUN mvn dependency:go-offline ${MAVEN_CLI_OPTS}

COPY src/ src/

RUN mvn package ${MAVEN_CLI_OPTS}

FROM openjdk:18-slim

ENV APP_HOME=/usr/app

WORKDIR ${APP_HOME}

RUN mkdir -p ${APP_HOME}

COPY --from=build /build/target/*.jar ${APP_HOME}/app.jar

RUN mkdir -p ${APP_HOME}/config

COPY --from=build /build/src/main/resources/application.yml ${APP_HOME}/config/application.yml

HEALTHCHECK --interval=30s --timeout=3s \
    CMD curl -f http://localhost:8080/actuator/health || exit 1

EXPOSE 8080

CMD ["java", "-jar", "app.jar", "--spring.config.location=file:/usr/app/config/"]
