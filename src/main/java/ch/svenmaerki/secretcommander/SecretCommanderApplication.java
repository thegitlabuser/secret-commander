package ch.svenmaerki.secretcommander;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecretCommanderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecretCommanderApplication.class, args);
    }

}
