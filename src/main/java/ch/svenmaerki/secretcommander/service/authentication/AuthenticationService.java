package ch.svenmaerki.secretcommander.service.authentication;

import ch.svenmaerki.secretcommander.domain.authentication.AuthenticationRequest;
import ch.svenmaerki.secretcommander.domain.authentication.AuthenticationResponse;
import ch.svenmaerki.secretcommander.domain.authentication.RegisterRequest;
import ch.svenmaerki.secretcommander.domain.authentication.Role;
import ch.svenmaerki.secretcommander.domain.entity.User;
import ch.svenmaerki.secretcommander.repository.entity.UserRepository;
import ch.svenmaerki.secretcommander.service.security.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    /**
     * Creates a new user and returns the authentication response containing the JWT token.
     *
     * @param request The request containing the user data.
     * @return The authentication response containing the JWT token.
     */
    public AuthenticationResponse register(RegisterRequest request) {
        User user = User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .username(request.getUsername())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
        repository.save(user);

        return generateAuthenticationResponse(user);
    }

    /**
     * Authenticates the user and returns the authentication response containing the JWT token.
     *
     * @param request The request containing the user credentials.
     * @return The authentication response containing the JWT token.
     */
    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );
        User user = repository.findByUsername(request.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return generateAuthenticationResponse(user);
    }

    /**
     * Refreshes the JWT token and returns the authentication response containing the new JWT token.
     *
     * @param token The JWT token.
     * @return The authentication response containing the new JWT token.
     */
    public AuthenticationResponse refresh(String token) {
        String username = jwtService.extractUsername(token);
        User user = repository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return generateAuthenticationResponse(user);
    }

    private AuthenticationResponse generateAuthenticationResponse(User user) {
        String jwtToken = jwtService.generateToken(user);

        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

}
