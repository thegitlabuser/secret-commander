package ch.svenmaerki.secretcommander.repository.entity;

import ch.svenmaerki.secretcommander.domain.entity.Secret;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SecretRepository extends JpaRepository<Secret, Long> {

    List<Secret> findAll();

}
