package ch.svenmaerki.secretcommander.service.entity;

import ch.svenmaerki.secretcommander.domain.entity.Secret;
import ch.svenmaerki.secretcommander.repository.entity.SecretRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SecretService {

    private final SecretRepository secretRepository;

    /**
     * Returns all secrets
     *
     * @return The secrets
     */
    public List<Secret> findAll() {
        return secretRepository.findAll();
    }

}
