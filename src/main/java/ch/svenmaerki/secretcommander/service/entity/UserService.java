package ch.svenmaerki.secretcommander.service.entity;

import ch.svenmaerki.secretcommander.domain.entity.User;
import ch.svenmaerki.secretcommander.repository.entity.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    /**
     * Returns all users
     *
     * @return The users
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Returns the user with the given id
     *
     * @param id The id
     * @return The user>
     */
    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("User with id " + id + " not found"));
    }

    /**
     * Returns the user with the given username
     *
     * @param username The username
     * @return The user
     */
    public User findByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " not found"));
    }

    /**
     * Returns the user with the given email
     *
     * @param email The email
     * @return The user
     */
    public User findByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User with email " + email + " not found"));
    }

    /**
     * Returns if a user with the given username exists
     *
     * @param username The username
     * @return True if the user exists, false otherwise
     */
    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    /**
     * Returns if a user with the given email exists
     *
     * @param email The email
     * @return True if the user exists, false otherwise
     */
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

}
