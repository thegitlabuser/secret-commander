package ch.svenmaerki.secretcommander.controller.system;

import ch.svenmaerki.secretcommander.domain.system.ApplicationContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.Instant;

import static ch.svenmaerki.secretcommander.utils.TimeUtils.formatDurationWords;

@Controller
@RequiredArgsConstructor
public class SystemSiteController {

    private final ApplicationContext applicationContext;

    @RequestMapping("/")
    public String showInfoPage(Model model) {
        model.addAttribute("applicationName", applicationContext.getName());
        model.addAttribute("version", applicationContext.getVersion());
        model.addAttribute("environment", applicationContext.getEnvironment());
        model.addAttribute("buildDate", applicationContext.getBuildDate());
        model.addAttribute("uptime", formatDurationWords(applicationContext.getUptime()));
        model.addAttribute("time", Instant.now());
        model.addAttribute("javaVersion", applicationContext.getJavaVersion());
        model.addAttribute("javaVendor", applicationContext.getJavaVendor());
        model.addAttribute("osName", applicationContext.getOsName());
        model.addAttribute("osVersion", applicationContext.getOsVersion());
        model.addAttribute("timezone", applicationContext.getTimezone());

        return "sites/info";
    }

}
