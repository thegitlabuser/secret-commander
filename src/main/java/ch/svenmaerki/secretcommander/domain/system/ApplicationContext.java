package ch.svenmaerki.secretcommander.domain.system;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

import java.lang.management.ManagementFactory;
import java.time.Duration;
import java.time.Instant;

@Getter
@Component
@RequiredArgsConstructor
public class ApplicationContext {

    private final BuildProperties buildProperties;

    @Value("${app.info.name}")
    private String name;

    @Value("${app.environment}")
    private String environment;

    public String getVersion() {
        return buildProperties.getVersion();
    }

    public Instant getBuildDate() {
        return buildProperties.getTime();
    }

    public Duration getUptime() {
        return Duration.ofMillis(ManagementFactory.getRuntimeMXBean().getUptime());
    }

    public String getJavaVersion() {
        return System.getProperty("java.version");
    }

    public String getJavaVendor() {
        return System.getProperty("java.vendor");
    }

    public String getOsName() {
        return System.getProperty("os.name");
    }

    public String getOsVersion() {
        return System.getProperty("os.version");
    }

    public String getTimezone() {
        return System.getProperty("user.timezone");
    }

}
