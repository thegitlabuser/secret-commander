package ch.svenmaerki.secretcommander.controller.entity;

import ch.svenmaerki.secretcommander.domain.ApplicationResponse;
import ch.svenmaerki.secretcommander.service.entity.AccessTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/access-tokens")
public class AccessTokenController {

    private final AccessTokenService accessTokenService;

    @GetMapping
    public ResponseEntity<ApplicationResponse> getAccessTokens() {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("All Access tokens fetched.")
                .data(accessTokenService.findAll())
                .build();

        return ResponseEntity.ok(response);
    }

}
