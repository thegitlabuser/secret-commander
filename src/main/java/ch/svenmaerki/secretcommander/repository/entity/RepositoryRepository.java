package ch.svenmaerki.secretcommander.repository.entity;

import ch.svenmaerki.secretcommander.domain.entity.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RepositoryRepository extends JpaRepository<Repository, Long> {

    List<Repository> findAll();

}
