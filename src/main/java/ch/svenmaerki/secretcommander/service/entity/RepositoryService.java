package ch.svenmaerki.secretcommander.service.entity;

import ch.svenmaerki.secretcommander.domain.entity.Repository;
import ch.svenmaerki.secretcommander.repository.entity.RepositoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RepositoryService {

    private final RepositoryRepository repositoryRepository;

    /**
     * Returns all repositories
     *
     * @return The repositories
     */
    public List<Repository> findAll() {
        return repositoryRepository.findAll();
    }

}
