package ch.svenmaerki.secretcommander.repository.entity;

import ch.svenmaerki.secretcommander.domain.entity.AccessToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccessTokenRepository extends JpaRepository<AccessToken, Long> {

    List<AccessToken> findAll();

}
