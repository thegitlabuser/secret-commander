package ch.svenmaerki.secretcommander.exception;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.UUID;

@Slf4j
@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleUnknownException(
            final Exception exception,
            final HttpServletRequest request
    ) {
        String guid = UUID.randomUUID().toString();
        var response = ApplicationExceptionResponse.builder()
                .guid(guid)
                .path(request.getRequestURI())
                .method(request.getMethod())
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Internal server error")
                .build();

        log.error("Error GUID={}; error message: {}", guid, exception.getMessage(), exception);

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<?> handleApplicationException(
            final ApplicationException exception,
            final HttpServletRequest request
    ) {
        String guid = UUID.randomUUID().toString();
        var response = ApplicationExceptionResponse.builder()
                .guid(guid)
                .path(request.getRequestURI())
                .method(request.getMethod())
                .statusCode(exception.getHttpStatus().value())
                .status(exception.getHttpStatus())
                .message(exception.getMessage())
                .build();

        log.error("Error GUID={}; error message: {}", guid, exception.getMessage(), exception);

        return new ResponseEntity<>(response, exception.getHttpStatus());
    }
}
