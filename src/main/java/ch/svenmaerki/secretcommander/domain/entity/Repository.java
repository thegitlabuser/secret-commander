package ch.svenmaerki.secretcommander.domain.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "repositories")
@Data
public class Repository {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true, length = 50)
    private String name;

    @Column(nullable = false, unique = true, length = 100)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ci_cd_platform_id", nullable = false)
    private CICDPlatform cicdPlatform;

    @Column(name = "read_me", nullable = false, length = 1000000)
    private String readMe;

}
