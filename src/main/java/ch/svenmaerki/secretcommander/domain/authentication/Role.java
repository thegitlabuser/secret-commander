package ch.svenmaerki.secretcommander.domain.authentication;

public enum Role {

    USER,
    ADMIN;

}
