package ch.svenmaerki.secretcommander.service.system;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

/**
 * Service class using Thymeleaf template engine
 *
 * @see http://www.thymeleaf.org
 */
@Service
@RequiredArgsConstructor
public class TemplatingService {

    private final TemplateEngine templateEngine;

    private String processTemplate(String templateName, Map<String, Object> contextMap) {
        Context context = new Context();
        contextMap.forEach(context::setVariable);
        return templateEngine.process(templateName, context);
    }


}
