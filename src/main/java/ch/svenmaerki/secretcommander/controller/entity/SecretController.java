package ch.svenmaerki.secretcommander.controller.entity;

import ch.svenmaerki.secretcommander.domain.ApplicationResponse;
import ch.svenmaerki.secretcommander.service.entity.SecretService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/secrets")
public class SecretController {

    private final SecretService secretService;

    @GetMapping
    public ResponseEntity<ApplicationResponse> getSecrets() {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("All secrets fetched.")
                .data(secretService.findAll())
                .build();

        return ResponseEntity.ok(response);
    }

}
