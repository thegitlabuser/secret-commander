package ch.svenmaerki.secretcommander.controller.entity;

import ch.svenmaerki.secretcommander.domain.ApplicationResponse;
import ch.svenmaerki.secretcommander.service.entity.CICDPlatformService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/platform")
public class CICDPlatformController {

    private final CICDPlatformService cicdPlatformService;

    @GetMapping
    public ResponseEntity<ApplicationResponse> getPlatforms() {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("All platforms fetched.")
                .data(cicdPlatformService.findAll())
                .build();

        return ResponseEntity.ok(response);
    }

}
