package ch.svenmaerki.secretcommander.controller.entity;

import ch.svenmaerki.secretcommander.domain.ApplicationResponse;
import ch.svenmaerki.secretcommander.service.entity.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<ApplicationResponse> getAllUsers() {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("All users fetched.")
                .data(userService.findAll())
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/id")
    public ResponseEntity<ApplicationResponse> getUserById(
            @RequestParam Long id
    ) {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("User with id " + id + " fetched.")
                .data(userService.findById(id))
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/username")
    public ResponseEntity<ApplicationResponse> getUserByUsername(
            @RequestParam String username
    ) {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("User with username " + username + " fetched.")
                .data(userService.findByUsername(username))
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/email")
    public ResponseEntity<ApplicationResponse> getUserByEmail(
            @RequestParam String email
    ) {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("User with email " + email + " fetched.")
                .data(userService.findByEmail(email))
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("username/exists")
    public ResponseEntity<ApplicationResponse> existsByUsername(
            @RequestParam String username
    ) {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("User with username " + username + " fetched.")
                .data(userService.existsByUsername(username))
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("email/exists")
    public ResponseEntity<ApplicationResponse> existsByEmail(
            @RequestParam String email
    ) {
        var response = ApplicationResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .message("User with email " + email + " fetched.")
                .data(userService.existsByEmail(email))
                .build();

        return ResponseEntity.ok(response);
    }

}
