package ch.svenmaerki.secretcommander.exception;

import ch.svenmaerki.secretcommander.domain.ApplicationResponse;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class ApplicationExceptionResponse extends ApplicationResponse {

    private final String guid;

    private final String path;

    private final String method;

}
