package ch.svenmaerki.secretcommander.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;

class TimeUtilsTest {

    @Test
    @DisplayName("Should format duration correctly")
    public void testFormatDurationWords() {
        // given
        Duration duration = Duration.ofSeconds(123456789);

        // when
        String result = TimeUtils.formatDurationWords(duration);

        // then
        Assertions.assertEquals("1428 days, 21 hours, 33 minutes, 9 seconds", result);
    }

    @Test
    @DisplayName("Should format duration with only seconds")
    public void testFormatDurationWordsWithOnlySeconds() {
        // given
        Duration duration = Duration.ofSeconds(45);

        // when
        String result = TimeUtils.formatDurationWords(duration);

        // then
        Assertions.assertEquals("45 seconds", result);
    }

    @Test
    @DisplayName("Should format duration with only minutes")
    public void testFormatDurationWordsWithOnlyMinutes() {
        // given
        Duration duration = Duration.ofMinutes(45);

        // when
        String result = TimeUtils.formatDurationWords(duration);

        // then
        Assertions.assertEquals("45 minutes, 0 seconds", result);
    }

    @Test
    @DisplayName("Should format duration with only hours")
    public void testFormatDurationWordsWithOnlyHours() {
        // given
        Duration duration = Duration.ofHours(5);

        // when
        String result = TimeUtils.formatDurationWords(duration);

        // then
        Assertions.assertEquals("5 hours, 0 seconds", result);
    }

    @Test
    @DisplayName("Should format duration with only days")
    public void testFormatDurationWordsWithOnlyDays() {
        // given
        Duration duration = Duration.ofDays(10);

        // when
        String result = TimeUtils.formatDurationWords(duration);

        // then
        Assertions.assertEquals("10 days, 0 seconds", result);
    }

    @Test
    @DisplayName("Should format duration with zero seconds")
    public void testFormatDurationWordsWithZeroDuration() {
        // given
        Duration duration = Duration.ZERO;

        // when
        String result = TimeUtils.formatDurationWords(duration);

        // then
        Assertions.assertEquals("0 seconds", result);
    }

}